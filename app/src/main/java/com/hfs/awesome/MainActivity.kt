package com.hfs.awesome

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.AppCompatButton
import com.hfs.lib.scan.HuaweiScanActivity

class MainActivity : AppCompatActivity() {

    private lateinit var mBtnScan: AppCompatButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mBtnScan = findViewById(R.id.btn_scan)
        mBtnScan.setOnClickListener {
            startActivity(Intent(this, HuaweiScanActivity::class.java))
        }

    }
}